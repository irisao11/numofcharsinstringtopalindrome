import java.util.*;

public class NumOfChar {

    public void verifyWordsInText(String text) {

        // creating an array that contains the chars in my text
        String toLowerText = text.toLowerCase();
        String[] stringToChar = toLowerText.split("");
        System.out.println(Arrays.toString(stringToChar));

        //creating an array of ints that will count how many times a  char has appears in my array of chars
        int[] counts = new int[stringToChar.length];
        int count = 0;
        int numOfChars = 0;
        for (int i = 0; i < stringToChar.length; i++) {
            for (int j = 0; j < stringToChar.length; j++) {
                if (stringToChar[i].equals(stringToChar[j])) {
                    count = count + 1;
                }
            }
            counts[i] = count;
            count = 0;
        }
        System.out.println(Arrays.toString(counts));

        //creating a set with all nonrepeating chars
        Set<String> nonRepeatingChars = new HashSet<>();
        for (int i = 0; i < counts.length; i++) {
            if (counts[i] % 2 != 0) {
                nonRepeatingChars.add(stringToChar[i]);
            }
        }
        int numOfOdds = nonRepeatingChars.size();

        //creating an array of Strings with all the chars that appear an odd number of times
        String[] nonRepeatedChars = nonRepeatingChars.toArray(new String[nonRepeatingChars.size()]);

        //creating a boolean variable to verify if a char appears an odd or even number of times
        boolean verifyIfOnlyOdNumbers = true;
        for (int i = 0; i < counts.length; i++) {
            if (counts[i] % 2 == 0) {
                verifyIfOnlyOdNumbers = false;
            }
        }

        //verifying all the cases and showing the number of chars and the chars
        //necessary for a String to be palindrome
        if (numOfOdds == 1) {
            System.out.println("Number of char necessary for palindrome is " + numOfChars);
            System.out.println("String is palindrome");
        } else if (numOfOdds % 2 == 0) {
            numOfChars = numOfOdds;
            System.out.println("Number of char necessary for palindrome is " + numOfChars);
            System.out.print("the letters are: ");
            for (int i = 0; i < counts.length; i++) {
                if (counts[i] % 2 != 0) {
                    System.out.print(stringToChar[i] + " ");
                }
            }
        } else if (numOfOdds % 2 != 0) {
            for (int i = 0; i < counts.length; i++) {
                if (!verifyIfOnlyOdNumbers) {
                    System.out.print("the letters are: ");
                    numOfChars = numOfOdds - 1;
                    for (int j = 0; j < nonRepeatedChars.length - 1; j++) {
                        System.out.print(nonRepeatedChars[j] + " ");
                    }
                    break;
                } else {
                    numOfChars = numOfOdds;
                    System.out.print("the letters are: ");
                    for (int j = 0; i < counts.length; i++) {
                        if (counts[i] % 2 != 0) {
                            System.out.print(stringToChar[i] + " ");
                        }
                    }
                }
            }
            System.out.println();
            System.out.println("Number of chars necessary for palindrome is " + numOfChars);
        }
    }
}
