public class MyString {
    private MyString myString;

    public MyString(){
    }

    public MyString(MyString myString) {
        this.myString = myString;
    }

    public MyString getMyString() {
        return myString;
    }

    public void setMyString(MyString myString) {
        this.myString = myString;
    }


}
